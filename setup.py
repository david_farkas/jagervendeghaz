#!/usr/bin/env python

from setuptools import setup

setup(
    # GETTING-STARTED: set your app name:
    name='JagerVendeghaz',
    # GETTING-STARTED: set your app version:
    version='0.1',
    # GETTING-STARTED: set your app description:
    description='Jager Vendeghaz Application',
    # GETTING-STARTED: set author name (your name):
    author='David Farkas',
    # GETTING-STARTED: set author email (your email):
    author_email='david.farkas93@gmail.com',
    # GETTING-STARTED: set author url (your url):
    url='http://www.python.org/sigs/distutils-sig/',
    # GETTING-STARTED: define required django version:
    install_requires=[
        'Django==1.8.2'
    ],
    dependency_links=[
        'https://pypi.python.org/simple/django/'
    ],
)
